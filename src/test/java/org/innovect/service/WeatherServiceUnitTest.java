package org.innovect.service;

import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import org.innovect.java.BackendAppRunner;
import org.innovect.java.domain.Forecast;
import org.innovect.java.service.WeatherService;
import org.innovecture.java.model.dto.ForecastResponseDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackendAppRunner.class)
public class WeatherServiceUnitTest {

	@Value("${weather.api.key}")
	private String apiKey;

	@Value("${weather.api.host}")
	private String apiHost;

	@Autowired
	private WeatherService weatherService;

	@MockBean
	private RestTemplate template;

	@Test
	public void testTomorrowForecast() throws FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader("src/test/resources/externalApiResponse.json"));

		Forecast foreCast = new Gson().fromJson(br, Forecast.class);
		String zipCode = "94040";
		String countryCode = "us";
		String url = apiHost + "forecast?" + "zip=" + zipCode + "," + countryCode + "&appid=" + apiKey;
		when(template.getForEntity(url, Forecast.class))
				.thenReturn(new ResponseEntity<Forecast>(foreCast, HttpStatus.OK));
		ForecastResponseDTO forecastObj = weatherService.getTomorrowForecast("94040", "us");
		Assert.assertNotNull(forecastObj);
	}

	@Test
	public void testForecastZipOnly() throws FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader("src/test/resources/externalApiResponse.json"));

		Forecast foreCast = new Gson().fromJson(br, Forecast.class);
		String zipCode = "94040";
		String countryCode = "";
		String url = apiHost + "forecast?" + "zip=" + zipCode + "," + countryCode + "&appid=" + apiKey;
		when(template.getForEntity(url, Forecast.class))
				.thenReturn(new ResponseEntity<Forecast>(foreCast, HttpStatus.OK));
		ForecastResponseDTO forecastObj = weatherService.getTomorrowForecast("94040", "");
		Assert.assertNotNull(forecastObj);
	}

	@Test(expected = RuntimeException.class)
	public void testForecastInvalidZip() {
		String zipCode = "1234567";
		String countryCode = "";
		String url = apiHost + "forecast?" + "zip=" + zipCode + "," + countryCode + "&appid=" + apiKey;
		when(template.getForEntity(url, Forecast.class)).thenThrow(new RuntimeException(""));
		weatherService.getTomorrowForecast("1234567", "");
	}

	@Test(expected = RuntimeException.class)
	public void testForecastAbsentZipCntryCd() {
		String zipCode = "";
		String countryCode = "";
		String url = apiHost + "forecast?" + "zip=" + zipCode + "," + countryCode + "&appid=" + apiKey;
		when(template.getForEntity(url, Forecast.class)).thenThrow(new RuntimeException(""));
		weatherService.getTomorrowForecast("", "");
	}
	
}
