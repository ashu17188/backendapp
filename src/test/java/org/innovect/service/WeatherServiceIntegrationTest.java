package org.innovect.service;

import org.innovect.java.BackendAppRunner;
import org.innovect.java.service.WeatherService;
import org.innovect.java.service.impl.WeatherServiceImpl;
import org.innovecture.java.model.dto.ForecastResponseDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackendAppRunner.class)
public class WeatherServiceIntegrationTest {

	@TestConfiguration
	static class GenericTestContextConfiguration {

		@Bean
		public WeatherService weatherService() {
			return new WeatherServiceImpl();
		}
	}

	@Autowired
	private WeatherService weatherService;

	@Test
	public void getTomorrowForecastTest() {
		ForecastResponseDTO forecastResponseDTO = weatherService.getTomorrowForecast("94040", "us");
		Assert.assertNotNull(forecastResponseDTO);
	}

	@Test
	public void getForecastWithCntryCdAbsentTest() {
		ForecastResponseDTO forecastResponseDTO = weatherService.getTomorrowForecast("94040", "");
		Assert.assertNotNull(forecastResponseDTO);
	}

	@Test(expected = HttpClientErrorException.class)
	public void getForecastWithInvalidZipCntryCdTest() {
		weatherService.getTomorrowForecast("94040", "in");
	}

	@Test(expected = HttpClientErrorException.class)
	public void getForecastWithZipAbsentTest() {
		ForecastResponseDTO forecastResponseDTO = weatherService.getTomorrowForecast("    ", "in");
		Assert.assertNotNull(forecastResponseDTO);
	}
}
