package org.innovect.java;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackendAppRunner.class)
@TestPropertySource("classpath:application.properties")
public class ApplicationTest {
    
	@Autowired
	private ApplicationContext applicationContext;
	
	@Test
    public void contextLoads() {
		Assert.assertNotNull(applicationContext);
    }
}
