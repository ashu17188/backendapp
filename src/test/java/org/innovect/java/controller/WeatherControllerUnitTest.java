package org.innovect.java.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.innovect.java.service.WeatherService;
import org.innovecture.java.model.dto.ForecastResponseDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@SpringBootTest
@RunWith(SpringRunner.class)
public class WeatherControllerUnitTest {

	MockMvc mockMvc;

	@Autowired
	private WeatherController weatherController;

	@MockBean
	private WeatherService weatherService;

	@Mock
	private RestTemplate restTemplate;

	private ForecastResponseDTO forecastResponseDTO;

	@Before
	public void setup() throws Exception {
		this.mockMvc = standaloneSetup(this.weatherController).build();
		forecastResponseDTO = new ForecastResponseDTO();
		forecastResponseDTO.setName("Federal Way");
		forecastResponseDTO.setCountryCode("US");

	}

	@Test
	public void testWeatherByZipCode() throws Exception {
		when(weatherService.getTomorrowForecast("98003", "")).thenReturn(forecastResponseDTO);
		mockMvc.perform(get("/api/forecast/98003?countryCode=").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}

	@Test
	public void testWeatherByZipCodeOnly() throws Exception {
		when(weatherService.getTomorrowForecast("98003", "")).thenReturn(forecastResponseDTO);
		mockMvc.perform(get("/api/forecast/98003").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	}

	@Test
	public void testWeatherByInvalidZip() throws Exception {
		when(weatherService.getTomorrowForecast("13412341", "us")).thenThrow(new ResponseStatusException(
				HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred while call weather api."));
		mockMvc.perform(get("/api/forecast/13412341").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(500));

	}
	
	@Test
	public void testWeatherByZipAbsent() throws Exception {
		when(weatherService.getTomorrowForecast(" ", "us")).thenThrow(new ResponseStatusException(
				HttpStatus.NOT_FOUND, "Zip is required."));
		mockMvc.perform(get("/api/forecast/  ").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(404));

	}

}
