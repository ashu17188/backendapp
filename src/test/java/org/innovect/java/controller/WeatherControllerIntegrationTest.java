package org.innovect.java.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@RunWith(SpringRunner.class)
public class WeatherControllerIntegrationTest {

	private MockMvc mockMvc;
	
	@Autowired
	private WeatherController weatherController;

	@Before
	public void setup() throws Exception {
		this.mockMvc = standaloneSetup(this.weatherController).build();
	}

	@Test
	public void testUSWeatherInfoByZipCodeOnly() throws Exception {
		mockMvc.perform(get("/api/forecast/94040").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andDo(print())
				.andReturn();
	}

	@Test
	public void testAllWeatherInfoByZipCodeCountryCode() throws Exception {
		mockMvc.perform(get("/api/forecast/411028?countryCode=IN").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andDo(print())
				.andReturn();
		
	}

	@Test
	public void testAllWeatherInfoAbsentZipCode() throws Exception {
		mockMvc.perform(get("/api/forecast/  ?").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(404))
				.andDo(print())
				.andReturn();
		
	}
	
	@Test
	public void testAllWeatherInfoInvalidZipCode() throws Exception {
		mockMvc.perform(get("/api/forecast/1122344445?").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(500))
				.andDo(print())
				.andReturn();
		
	}
}
