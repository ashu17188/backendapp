package org.innovect.java.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@WebMvcTest(WelcomeController.class)
public class WelcomeControllerTest {

	@Autowired
	private MockMvc mockMvc;

	Map<String,Object> map = new HashMap<>();
	
	@Before
	public void setUp() {
	    map.put("welcomeMsg","WelcomeController executed");
	}

	@Test
	public void testWeatherApiByCity() throws IOException, Exception {
		String value = map.get("welcomeMsg").toString();
		ResultActions ra = mockMvc.perform(get("/").contentType(MediaType.APPLICATION_JSON)
				.content(value));
		ra.andExpect(status().isOk());
	}

	
}
