package org.innovect.java.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DailyClimate {
	private String dt;
	private Main main;
	private List<Weather> weather;
	private Cloud clouds;
	private Wind wind;
	private SystemInfo sys;

	@JsonProperty("dt_txt")
	private String dailyTxt;

	public String getDt() {
		return dt;
	}

	public void setDt(String dt) {
		this.dt = dt;
	}

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

	public List<Weather> getWeather() {
		return weather;
	}

	public void setWeather(List<Weather> weather) {
		this.weather = weather;
	}

	public Cloud getClouds() {
		return clouds;
	}

	public void setClouds(Cloud clouds) {
		this.clouds = clouds;
	}

	public Wind getWind() {
		return wind;
	}

	public void setWind(Wind wind) {
		this.wind = wind;
	}

	public SystemInfo getSys() {
		return sys;
	}

	public void setSys(SystemInfo sys) {
		this.sys = sys;
	}

	public String getDailyTxt() {
		return dailyTxt;
	}

	public void setDailyTxt(String dailyTxt) {
		this.dailyTxt = dailyTxt;
	}

}
