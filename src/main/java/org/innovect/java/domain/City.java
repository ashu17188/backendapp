package org.innovect.java.domain;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class City {
	private String name;
	private Coordinate coord;
	private String country;
	private String timezone;
	private String sunrise;
	private String sunset;

	/**
	 * 
	 * @param timeStamp
	 * @return java.util.Date from unix timestamp
	 */
	public String getUnixTimeStampToDate(String timeStamp) {
		long timeTemp = Long.parseLong(timeStamp);
		Date date = new java.util.Date(timeTemp * 1000);
		return date.toString();
	}

	/**
	 * 
	 * @param offsetStr
	 * @return timezone in String format with respect to GMT value;
	 */
	public String getTimeZone() {
		long offset = Long.parseLong(timezone); // offset IST (seconds)
		long time = TimeUnit.SECONDS.toMinutes(offset); // or offset/60
		long hour = time / 60;
		long min = Math.abs(time % 60);
		String hrStr = "";
		if (hour > 0 && hour < 10) {
			hrStr = "+0" + hour;
		} else if (hour >= 10) {
			hrStr = "+" + hour;
		} else if (hour < 0 && hour > -10) {
			hrStr = "-0" + String.valueOf(hour).substring(1);
		} else {
			hrStr = String.valueOf(hour);
		}
		String minStr = String.valueOf(min);
		if (min < 10) {
			minStr = "0" + (time % 60);
		}
		return (hrStr + ":" + minStr);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Coordinate getCoord() {
		return coord;
	}

	public void setCoord(Coordinate coord) {
		this.coord = coord;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getSunrise() {
		return sunrise;
	}

	public void setSunrise(String sunrise) {
		this.sunrise = sunrise;
	}

	public String getSunset() {
		return sunset;
	}

	public void setSunset(String sunset) {
		this.sunset = sunset;
	}
	
	
}
