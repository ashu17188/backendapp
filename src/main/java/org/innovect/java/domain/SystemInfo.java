package org.innovect.java.domain;

public class SystemInfo {
	private String pod;

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

}
