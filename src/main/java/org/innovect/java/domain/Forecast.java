
package org.innovect.java.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Forecast {

	private String cod;
	private String message;
	private String cnt;
	private List<DailyClimate> list;
	private City city;

	public List<DailyClimate> getTomorrowClimate() {
		List<DailyClimate> tempList = new ArrayList<>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDate tomorrow = LocalDate.now().plusDays(1);
		list.stream().forEach(dailyClimate -> {
			LocalDate localDate = LocalDate.parse(dailyClimate.getDailyTxt(), formatter);
			if (localDate.equals(tomorrow)) {
				tempList.add(dailyClimate);
			}
		});
		return tempList;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCnt() {
		return cnt;
	}

	public void setCnt(String cnt) {
		this.cnt = cnt;
	}

	public List<DailyClimate> getList() {
		return list;
	}

	public void setList(List<DailyClimate> list) {
		this.list = list;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

}
