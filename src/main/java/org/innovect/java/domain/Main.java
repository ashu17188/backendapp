package org.innovect.java.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Main {
	private String temp;

	@JsonProperty("temp_min")
	private String tempMin;

	@JsonProperty("temp_max")
	private String tempMax;

	private String pressure;

	@JsonProperty("sea_level")
	private String seaLevel;

	@JsonProperty("grnd_level")
	private String grndLevel;

	private String humidity;

	@JsonProperty("temp_kf")
	private String tempKf;

	/**
	 * @param kelvin temperature in Kelvin as a float
	 * @return temperature in Celcius in float datatype
	 */
	public float kelvinToCelcius(float kelvin) {

		return kelvin - 273.15F;
	}

	/**
	 * @param kelvinT temperature in Kelvin
	 * @return String temperature in celcius
	 */
	public String kelvinToCelcius(String kelvinT) {
		float kelvin = Float.parseFloat(kelvinT);
		return String.valueOf(kelvin - 273.15F);

	}

	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getTempMin() {
		return tempMin;
	}

	public void setTempMin(String tempMin) {
		this.tempMin = tempMin;
	}

	public String getTempMax() {
		return tempMax;
	}

	public void setTempMax(String tempMax) {
		this.tempMax = tempMax;
	}

	public String getPressure() {
		return pressure;
	}

	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	public String getSeaLevel() {
		return seaLevel;
	}

	public void setSeaLevel(String seaLevel) {
		this.seaLevel = seaLevel;
	}

	public String getGrndLevel() {
		return grndLevel;
	}

	public void setGrndLevel(String grndLevel) {
		this.grndLevel = grndLevel;
	}

	public String getHumidity() {
		return humidity;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	public String getTempKf() {
		return tempKf;
	}

	public void setTempKf(String tempKf) {
		this.tempKf = tempKf;
	}

}
