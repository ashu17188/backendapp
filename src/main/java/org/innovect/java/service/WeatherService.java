package org.innovect.java.service;

import org.innovecture.java.model.dto.ForecastResponseDTO;

public interface WeatherService {

	/**
	 * This method accepts Zip code and Country code of same country for doing api
	 * call.
	 * @param zipCode  Zip code of any country.
	 * @param countryCode Country code of country.
	 * @return ForecastResponseDTO- custom user object- after External weather api call.
	 */
	ForecastResponseDTO getTomorrowForecast(String zipCode, String countryCode);
}
