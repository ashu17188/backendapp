package org.innovect.java.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.innovect.java.domain.Forecast;
import org.innovect.java.logging.Loggable;
import org.innovect.java.service.WeatherService;
import org.innovecture.java.model.dto.DailyClimateDTO;
import org.innovecture.java.model.dto.ForecastResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherServiceImpl implements WeatherService {

	@Value("${weather.api.key}")
	private String apiKey;

	@Value("${weather.api.host}")
	private String apiHost;

	Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Loggable
	public ForecastResponseDTO getTomorrowForecast(String zipCode, String countryCode) {
		String url = apiHost + "forecast?" + "zip=" + zipCode + "," + countryCode + "&appid=" + apiKey;
		ResponseEntity<Forecast> response = restTemplate.getForEntity(url, Forecast.class);
		if (HttpStatus.OK == response.getStatusCode()) {
			log.info("Response:{}", response.getBody());
		}

		return mapper(response.getBody());
	}
	
	/**
	 * @param forecast Data returned from external Weather Api call.
	 * @return DTO object which is specific to end user api call
	 */
	public ForecastResponseDTO mapper(Forecast forecast) {
		ForecastResponseDTO forecastResponseDTO = new ForecastResponseDTO();
		forecastResponseDTO.setName(forecast.getCity().getName());
		forecastResponseDTO.setLatitude(forecast.getCity().getCoord().getLat());
		forecastResponseDTO.setLongitude(forecast.getCity().getCoord().getLon());
		forecastResponseDTO.setSunrise(forecast.getCity().getUnixTimeStampToDate(forecast.getCity().getSunrise()));
		forecastResponseDTO.setSunset(forecast.getCity().getUnixTimeStampToDate(forecast.getCity().getSunset()));
		forecastResponseDTO.setTimezone(forecast.getCity().getTimeZone());
		forecastResponseDTO.setCountryCode(forecast.getCity().getCountry());
		
		List<DailyClimateDTO> dcList = new ArrayList<>();
		forecast.getTomorrowClimate().forEach(dailyClimate->{
			DailyClimateDTO dailyClimateDTO  = new DailyClimateDTO();
			dailyClimateDTO.setRecordedDateTime(dailyClimate.getDailyTxt());
			dailyClimateDTO.setTemp(dailyClimate.getMain().kelvinToCelcius(dailyClimate.getMain().getTemp()));
			dailyClimateDTO.setTempMin(dailyClimate.getMain().kelvinToCelcius(dailyClimate.getMain().getTempMin()));
			dailyClimateDTO.setTempMax(dailyClimate.getMain().kelvinToCelcius(dailyClimate.getMain().getTempMax()));
			dailyClimateDTO.setPressure(dailyClimate.getMain().getPressure());
			StringBuilder desc = new StringBuilder();
			dailyClimate.getWeather().forEach(weather ->{
				desc.append(weather.getDescription());
				desc.append(",");
			});
			dailyClimateDTO.setDescription(desc.toString().replaceAll(",$", ""));
			dcList.add(dailyClimateDTO);
		});
		forecastResponseDTO.setList(dcList);
		
		return forecastResponseDTO;
	}
}

	