package org.innovect.java.controller;

import java.util.Optional;

import org.innovect.java.logging.Loggable;
import org.innovect.java.service.WeatherService;
import org.innovecture.java.model.dto.ForecastResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Ashutosh Shukla
 */

@RestController
@RequestMapping("/api")
@Api(value = "Tomorrow's Weather Information")
public class WeatherController {

	Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private WeatherService weatherService;

	@Value("${spring.defaultCountryCode}")
	private String defaultCountryCode;

	/**
	 * @param zipCode     Mandatory parameter
	 * @param countryCode Optional parameter,If not passed "us" will be default
	 *                    countryCode
	 * @return tomorrow's weather in every 3 hours of interval
	 */
	@Loggable
	@ApiOperation(value = "View tomorrow's weather information for every 3 hours", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved tomorrow's weather info."),
			@ApiResponse(code = 500, message = "Something problem occurred from server side."),
			@ApiResponse(code = 404, message = "Parameter missing in api call.") })
	@GetMapping("/forecast/{zipCode}")
	public ResponseEntity<ForecastResponseDTO> getForecastWeatherByZipCode(@PathVariable("zipCode") String zipCode,
			@RequestParam("countryCode") Optional<String> countryCodeOptional) {
		if (!countryCodeOptional.isPresent() || StringUtils.isBlank(countryCodeOptional.get())) {
			countryCodeOptional = Optional.of(defaultCountryCode);
			log.info("countryCode is empty. Setting countryCode to:{}", defaultCountryCode);
		}
		if (StringUtils.isBlank(zipCode)) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Zip Code is required.");
		}
		log.info("Zip Code:{}, Country Code:{}", zipCode, countryCodeOptional.get());
		ForecastResponseDTO forecastResponseDTO = new ForecastResponseDTO();
		try {
			forecastResponseDTO = weatherService.getTomorrowForecast(zipCode, countryCodeOptional.get());
		} catch (RuntimeException re) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Error occurred while call weather api.");
		}
		return new ResponseEntity<>(forecastResponseDTO, HttpStatus.OK);
	}

}
