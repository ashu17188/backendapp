package org.innovect.java.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/")
@Api(value="welcome page gives basic information.")
public class WelcomeController {

	@Autowired
    Environment environment;
	
	 
	@GetMapping(value="/")
	public Map<String,Object> getWelcomeMsg(){
	    Map<String,Object> map = new HashMap<>();
	    map.put("welcomeMsg","WelcomeController executed");
	    map.put("Environment selected",environment.getActiveProfiles());
	    return map;
	}
}

