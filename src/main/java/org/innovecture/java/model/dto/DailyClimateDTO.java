package org.innovecture.java.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


public class DailyClimateDTO {
	private String temp;

	@JsonProperty("temp_min")
	private String tempMin;

	@JsonProperty("temp_max")
	private String tempMax;

	private String pressure;
	private String description;
	private String recordedDateTime;
	public String getTemp() {
		return temp;
	}
	public void setTemp(String temp) {
		this.temp = temp;
	}
	public String getTempMin() {
		return tempMin;
	}
	public void setTempMin(String tempMin) {
		this.tempMin = tempMin;
	}
	public String getTempMax() {
		return tempMax;
	}
	public void setTempMax(String tempMax) {
		this.tempMax = tempMax;
	}
	public String getPressure() {
		return pressure;
	}
	public void setPressure(String pressure) {
		this.pressure = pressure;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRecordedDateTime() {
		return recordedDateTime;
	}
	public void setRecordedDateTime(String recordedDateTime) {
		this.recordedDateTime = recordedDateTime;
	}


}
