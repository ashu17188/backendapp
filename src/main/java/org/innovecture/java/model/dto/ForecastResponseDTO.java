package org.innovecture.java.model.dto;

import java.util.List;

public class ForecastResponseDTO {
	
	private String name;
	private String latitude;
	private String longitude;
	private String countryCode;
	private String timezone;
	private String sunrise;
	private String sunset;
	
	private List<DailyClimateDTO> list;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getSunrise() {
		return sunrise;
	}

	public void setSunrise(String sunrise) {
		this.sunrise = sunrise;
	}

	public String getSunset() {
		return sunset;
	}

	public void setSunset(String sunset) {
		this.sunset = sunset;
	}

	public List<DailyClimateDTO> getList() {
		return list;
	}

	public void setList(List<DailyClimateDTO> list) {
		this.list = list;
	}

	
	
}
