---

## Project Introduction.
This project gives Weather information [https://home.openweathermap.org] for any location on Earth including over 200,000 cities.

---
## Project Features.
1.Latest Spring Boot 2.1.8. Compatible with Oracle Java8 and OpenJdk 8.

2.Swagger to test Rest Api.

3.H2 embedded database integrated for fast development.

4.Separate log folder having log file which can be utilised for Splunk integration.


---


## Project Setup

1.clone repository : **git clone https://ashu17188@bitbucket.org/ashu17188/backendapp.git**

2.Run command **mvn clean install**

3.Run **BackendAppRunner.java**  as Java Application

3.Swagger url: **http://localhost:8081/backendApp/swagger-ui.html**

4.H2 Database url: **http://localhost:8081/backendApp/h2-console**

---
## Api Description:
http://localhost:8081/backendApp/api/forecast/411028?countryCode=in

Zip code:411028 [Mandatory]

Country code: in [Optional,If not present US will be considered default Country Code]


---
1.pressure unit hPa
2.temperature unit celcius
